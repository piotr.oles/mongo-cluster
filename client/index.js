const { MongoClient } = require('mongodb');
const process = require('process');

async function wait(timeout) {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}

async function failover(callback, maxTries = 60, tryInterval = 1000) {
  let tries = 0;

  while (true) {
    try {
      return await callback();
    } catch (e) {
      tries++;
      console.error(
        `Failover method "${callback.toString()}"\n\tbecause of "${e.toString()}"\n\t(try ${tries} of ${maxTries}).`
      );

      if (tries >= maxTries) {
        throw e;
      }

      await wait(tryInterval);
    }
  }
}

async function connect(url) {
  console.log(`Connecting to ${url}...`);
  const client = await failover(async () => MongoClient.connect(url));
  console.log('Connected successfully.');

  return client;
}

async function configureShardCollection(client, dbName, collectionName, shardKeyName) {
  console.log(`Creating ${dbName} database...`);
  const db = await failover(async () => await client.db(dbName));

  console.log(`Enable sharding for ${dbName} database...`);
  await failover(async () =>
    await db.admin().command({ enableSharding: dbName })
  );

  console.log(`Creating ${dbName}.${collectionName} collection...`);
  await failover(async () =>
    await client.db(dbName, { returnNonCachedInstance: true }).createCollection(collectionName)
  );

  console.log(
    `Enable sharding for ${dbName}.${collectionName} collection by ${shardKeyName} key with 'hashed' strategy...`
  );
  await failover(
    async () => await db.admin().command({
      shardCollection: `${dbName}.${collectionName}`,
      key: {
        [shardKeyName]: 'hashed'
      },
      collation: {
        locale: "simple"
      }
    })
  );

  console.log('Sharding setup successful.');
  return await db.collection(collectionName);
}

async function producer(collection) {
  while (true) {
    const start = Date.now();
    const document = {
      example: Math.round(Math.random() * 1000)
    };

    // try to insert with failover
    await failover(async () => await collection.insert(document));

    const stop = Date.now();

    console.log(`Inserted ${JSON.stringify(document)} (${stop - start}ms)`);
    await wait(500);
  }
}

async function main() {
  const client = await connect(process.env.MONGO_URL);
  const collection = await configureShardCollection(client, 'agh', 'test', '_id');

  await producer(collection);
}

main();
