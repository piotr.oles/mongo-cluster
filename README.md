# Mongo Cluster on docker
> Cluster configuration with simple node client to test cluster's availability

In order to provide high availability and scalability, mongo provides cluster
mechanism. To test how cluster will behave, we've configured 2 shards with
3 servers in each shard (replica set), 1 configuration replica set 
(also 3 servers) and mongo router.

## Docker configuration
In the `docker` directory we have `Dockerfile`s with all required files 
(like entrypoint.sh). In this directory we have:
 * `client` - definition of `node:8.11.0` image for our node client.
 * `cluster` - definition of `mongo:3.6.3` image for mongo router container
   (in production environments it's recommended to host mongo router on the same
   machine as application is - to simplify configuration we decide to put it on
   a separate container)
 * `config/primary` - definition of default primary node in a configuration
   replica set
 * `config/secondary` - definition of a default secondary node in a configuration
   replica set
 * `data/primary` - definition of a default primary node in a data shard replica
   set
 * `data/secondary` - definition of a default secondary node in a configuration
   shard replica set
   
Dockerfiles are pretty simple - most of them copies entryfiles and starts them
with a mongo or node process as an argument.

## Docker compose configuration
Containers configuration is defined in the `docker-compose.yml` file. Containers
use the same network.
Services are:
 * `rs_data_0_0` - shard #0, node #0, default primary (priority 3)
 * `rs_data_0_1` - shard #0, node #1, default secondary (priority 2)
 * `rs_data_0_2` - shard #0, node #2, default secondary (priority 2)
 * `rs_data_1_0` - shard #1, node #0, default primary (priority 3)
 * `rs_data_1_1` - shard #1, node #1, default secondary (priority 2)
 * `rs_data_1_2` - shard #1, node #2, default secondary (priority 2)
 * `rs_cfg_0` - configuration replica set, node #0, default primary (priority 3)
 * `rs_cfg_1` - configuration replica set, node #1, default secondary (priority 2)
 * `rs_cfg_2` - configuration replica set, node #1, default secondary (priority 2)
 * `cluster` - mongo router
 * `client` - node client


## How to start mongo cluster
To start mongo cluster, type:
```
docker-compose up -d --build
```
To see container logs, type:
```
docker-compose logs -f <service_name>
```

## Client app

Aplikacji klienckiej odpowiada za stworzenie klienta bazy mongoDB, następnie konfiguracja shardowanych kolekcji.
Kolejnym jej zadaniem zapisywanie danych do wcześniej skonfigurowanych shardowanych kolekcji oraz zbadanie czasu takiego zapisu.
 
Aplikacja składa się z następujących funkcji

 * `main` - stworzenie clienta bazy mongo, stworzenie shareded collection oraz wywolanie funkcji producera
 * `producer` - zapisuje do bazy przykladowy dokument, mierząc i logując czas tej operacji
 * `configureShardCollection` - wykonuje serie operacji na bazie prowadzacych do skonfigurowania sharded collection
 * `connect` - zwraca klienta bazy mongoDB
 * `failover` - odpowiada za sprawdzenie czy przekazana operacja zakonczyła się sukcesem, w innym przypadku próbuje ponowić jej wykonanie przez podaną ilość prób
 * `wait` - blokuje wykonanie programu przez podany czas
