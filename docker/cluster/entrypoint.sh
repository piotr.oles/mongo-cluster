#!/bin/bash

# define mongo entrypoint
MONGO_ENTRYPOINT="
sh.addShard(\"rs_data_0/rs_data_0_0:27017,rs_data_0_1:27017,rs_data_0_2:27017\");
sh.addShard(\"rs_data_1/rs_data_1_0:27017,rs_data_1_1:27017,rs_data_1_2:27017\");
";

echo "Starting mongo cluster...";
echo "Config replica set: rs_cfg";
echo "Mongo entrypoint: $MONGO_ENTRYPOINT";

# create entrypoint.js file
echo $MONGO_ENTRYPOINT > entrypoint.js

# start mongos process in the background
exec "$@" --configdb rs_cfg/rs_cfg_0:27019,rs_cfg_1:27019,rs_cfg_2:27019 --port 27017 --bind_ip_all & export MONGOS_PID=$!;

# wait for mongos to initialize
sleep 30

# execute mongo entrypoint
mongo --port 27017 entrypoint.js;

# show mongos logs
tail -f /proc/$MONGOS_PID/fd/1
