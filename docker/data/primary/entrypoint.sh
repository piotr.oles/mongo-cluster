#!/bin/bash

# define mongo entrypoint
MONGO_ENTRYPOINT="
rs.initiate({
   _id: \"${REPLICA_SET}\",
   version: 1,
   members: [
      { _id: 0, host: \"${REPLICA_SET}_0:27017\", priority: 3 },
      { _id: 1, host: \"${REPLICA_SET}_1:27017\", priority: 2 },
      { _id: 2, host: \"${REPLICA_SET}_2:27017\", priority: 2 }
   ]
});
"

echo "Starting mongo primary...";
echo "Replica set: $REPLICA_SET";
echo "Mongo entrypoint: $MONGO_ENTRYPOINT";

# create entrypoint.js file
echo $MONGO_ENTRYPOINT > entrypoint.js;

# start mongod process in the background
exec "$@" --shardsvr --replSet $REPLICA_SET --port=27017 --bind_ip_all & export MONGOD_PID=$!

# wait for mongod to initialize
sleep 5

# execute mongo entrypoint
mongo --port 27017 entrypoint.js;

# show mongod logs
tail -f /proc/$MONGOD_PID/fd/1
