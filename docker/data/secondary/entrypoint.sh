#!/bin/bash

echo "Starting mongo secondary...";
echo "Replica set: $REPLICA_SET";

exec "$@" --replSet $REPLICA_SET --shardsvr --port 27017 --bind_ip_all;
