#!/bin/bash

echo "Starting mongo config...";
echo "Replica set: rs_cfg";

exec "$@" -configsvr --replSet rs_cfg --port 27019 --bind_ip_all
