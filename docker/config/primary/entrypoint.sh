#!/bin/bash

# define mongo entrypoint
MONGO_ENTRYPOINT="
rs.initiate({
   _id : \"rs_cfg\",
   configsvr: true,
   members: [
      { _id: 0, host: \"rs_cfg_0:27019\", \"priority\": 3 },
      { _id: 1, host: \"rs_cfg_1:27019\", \"priority\": 2 },
      { _id: 2, host: \"rs_cfg_2:27019\", \"priority\": 2 }
   ]
});
"

echo "Starting mongo config...";
echo "Replica set: rs_cfg";
echo "Mongo entrypoint: $MONGO_ENTRYPOINT";

# create entrypoint.js file
echo $MONGO_ENTRYPOINT > entrypoint.js;

# start mongod process in the background
exec "$@" --configsvr --replSet rs_cfg --port 27019 --bind_ip_all & export MONGOD_PID=$!

# wait for mongod to initialize
sleep 5

# execute mongo entrypoint
mongo --port 27019 entrypoint.js;

# show mongod logs
tail -f /proc/$MONGOD_PID/fd/1
